section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
  .counter:
    cmp  byte [rdi], 0
    je   .end
    inc  rdi
    jmp  .counter
  .end:
    sub  rdi, rax
    mov  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
;save rsp, divide in loop save rdx on stack, count iterations, print
    mov r11, rsp
    mov rax, rdi
    mov rcx, 10
    xor rdx, rdx
    push 0
    .loop:
        div rcx
        add dl, '0'
        dec rsp
        mov byte [rsp], dl
        xor rdx, rdx
        cmp rax, 0
        jnz .loop
    mov rdi, rsp
    push r11
    call print_string
    pop rsp
    ret
; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .ge0
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .ge0:
        call print_uint
    ret
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    mov rax, 1
;make loop, check every symbol
    .loop:
        mov cl, byte [rdi]
        cmp cl, byte [rsi]
        jne .end
        cmp cl, 0
        je .ret
        xor rcx, rcx
        inc rdi
        inc rsi
        jmp .loop
    .end:
        xor rax, rax
    .ret:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
;get adress and size, check whitespace, read symbols, if size more then buffer size 0 -> rax and ret, else read full word and then put zero in the end
    push r12
    push r13
    push r14
    mov r12, rdi
    xor r13, r13
    mov r14, rsi

.skip_whitespace:
    call read_char
    cmp rax, 0x20
    je .skip_whitespace
    cmp rax, 0x09
    je .skip_whitespace
    cmp rax, 0x0A
    je .skip_whitespace

.loop:
    cmp r13, r14
    je .buffer_full

    cmp al, 0
    je .done
   cmp al, 0x20
    je .done
    cmp al, 0x09
    je .done
    cmp al, 0x0A
    je .done


    mov byte [r12+r13], al
    inc r13
    call read_char
    jmp .loop

.buffer_full:
    xor rax, rax
    pop r14
    pop r13
    pop r12
    ret

.done:
    mov byte [r12+r13], 0
    mov rax, r12
    mov rdx, r13
    pop r14
    pop r13
    pop r12
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    mov r11, 10
    .loop:
        mov cl, byte [rdi+rdx]
        sub cl, '0'
        jl .E
        cmp cl, 9
        jg .E
        push rdx
        mul r11
        pop rdx
        inc rdx
        add rax, rcx
        jmp .loop

    .E:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r9, r9
    cmp byte [rdi], '-'
    jne parse_uint

    push rdi
    inc rdi
    call parse_uint
    pop rdi
    inc rdx
    neg rax
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
;rdi - string, rsi - buffer, rdx - length
    xor rax, rax
    .loop:
        cmp rax, rdx
        jge .wrong_length
        mov cl, byte [rdi + rax]
        mov byte [rsi + rax], cl
        cmp cl, 0
        je .done
        inc rax
        jmp .loop
    .wrong_length:
        xor rax, rax
    .done:
        ret
